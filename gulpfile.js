'use strict';

var gulp        = require('gulp'),
    watch       = require('gulp-watch'),
    prefixer    = require('gulp-autoprefixer'),
    sass        = require('gulp-sass'),
    csscomb     = require('gulp-csscomb'),
    browserSync = require('browser-sync'),
    reload      = browserSync.reload;



var path = {
    dist: {
        html: 'dist/',
        js: 'dist/js/',
        css: 'dist/css/',
    },
    src: {
        html: 'src/*.html',
        css: 'src/main.scss',
        js: 'src/*.js',
    },
    watch: {
        js: 'src/**/*.js',
        html: 'src/**/*.html',
        css: 'src/**/*.scss',
    },
    clean: './dist'
};


var config = {
    server: {
        baseDir: './dist'
    },
    tunnel: true,
    host: 'localhost',
    port: 9000,
    logPrefix: 'Postnov.Frontend'
};




gulp.task('html:build', function() {
    return gulp.src(path.src.html)
        .pipe(gulp.dest(path.dist.html))
        .pipe(reload({ stream: true }));
})


gulp.task('js:build', function() {
    return gulp.src(path.src.js)
        .pipe(gulp.dest(path.dist.js))
        .pipe(reload({ stream: true }));
})



gulp.task('css:build', function() {
    return gulp.src(path.src.css)
        .pipe(sass())
        .pipe(prefixer({
            browsers: ['last 15 versions'],
            cascade: false,
            grid: true
        }))
        .pipe(csscomb())
        .pipe(gulp.dest(path.dist.css))
        .pipe(reload({ stream: true }));
});


gulp.task('build', [
    'html:build',
    'css:build',
    'js:build',
])


gulp.task('watch', ['css:build', 'js:build', 'html:build'], function () {
    gulp.watch(path.watch.css, ['css:build'])
    gulp.watch(path.watch.js, ['js:build'])
    gulp.watch(path.watch.html, ['html:build'])
})

gulp.task('webserver', function() {
    browserSync(config);
});

gulp.task('default', ['build', 'webserver', 'watch']);
