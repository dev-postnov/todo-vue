;
(function () {



    var data = {
        todos: (localStorage.todos) ? JSON.parse(localStorage.todos) : {
            inbox: [
                {
                    title: 'Задача 1',
                    descr: 'Вы можете редактировать описание или название задачи просто кликнув на них! Чтобы сохранить изменения кликните вне поля или нажмите "Enter"',
                }
            ],
            done: [
                {
                    title: 'Выполненная задача',
                    descr: 'Описание выполненной задачи задачи',
                }
            ],
        },
        delete_is_visible: false,
        detail_is_visible: false,
        activeTaskData: {}
    };



    var detail = Vue.component('detail-task', {
        template: '#detail',
        data: function () {
            return data
        },
        methods: {
            saveTitle: function () {
                this.activeTaskData.titleEditing = false;
                saveLocalStorage(this);
            },
            saveDescr: function () {
                this.activeTaskData.descrEditing = false;
                saveLocalStorage(this);
            },
            taskDone: function (index) {
                this.detail_is_visible = false;
                var task = this.todos.inbox.splice(index, 1);
                this.todos.done.unshift(task[0]);
                saveLocalStorage(this);
            },
        }
    })



    var modalDelete = Vue.component('modal-delete', {
        template: '#modal-delete',
        data: function () {
            return data
        },
        methods: {
            taskDelete: function (activeTaskData) {
                activeTaskData.arr.splice(activeTaskData.index, 1);
                this.delete_is_visible = false;
                saveLocalStorage(this)
            },
        }

    })




    var app = new Vue({
        el: '#app',
        data: data,
        methods: {
            addTask: function (e) {
                var typeEnter = (e.which === 13),
                    value = e.target.value;

                if (typeEnter && value != '') {
                    this.todos['inbox'].push({
                        title: e.target.value
                    })

                    e.target.value = '';

                    saveLocalStorage(this);
                }
            },
            taskDone: function (index) {
                var task = this.todos.inbox.splice(index, 1);
                this.todos.done.unshift(task[0]);
                saveLocalStorage(this);
            },

            taskReturn: function (index) {
                var task = this.todos.done.splice(index, 1);
                this.todos.inbox.push(task[0])
                saveLocalStorage(this);
            },

            showModalDelete: function (index, arr) {
                this.activeTaskData = {
                    arr: arr,
                    index: index,
                    title: arr[index]['title']
                };

                this.delete_is_visible = true;
            },

            showDetail: function (index, arr) {
                this.activeTaskData = {
                    arr: arr,
                    index: index,
                    title: arr[index]['title'],
                    descr: arr[index]['descr'],
                    descrEditing: false,
                    titleEditing: false
                };

                this.detail_is_visible = true;
            },
            closeDetail: function (e) {
                var closestDetail = e.target.closest('.detail-task');

                if (this.detail_is_visible && closestDetail == null) {
                    this.detail_is_visible = false;
                }
            }

        },

    });


    function saveLocalStorage(self) {
        localStorage.todos = JSON.stringify(self.todos);
    }


})();